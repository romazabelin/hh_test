@extends('layouts.main')

<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-6 col-md-4 offset-md-4">
            <div class="card">
                <div class="card-block">
                    <form action="{{route('sign.in')}}" method="post">
                        <div class="row">
                            <div class="col-sm-12 col-md-10  offset-md-1 ">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="{{trans('login.username')}}" name="username">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" placeholder="{{trans('login.password')}}" name="password">
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="{{trans('login.sign_in')}}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


