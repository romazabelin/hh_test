<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');
Route::get('/login', array('as' => 'login', 'uses' => 'AuthController@login'));
Route::get('/tasks', array('as' => 'tasks', 'uses' => 'TaskController@index'));
Route::match(['get', 'post'], '/new_task', array('as' => 'new.task', 'uses' => 'TaskController@create'));

Route::post('/signin', array('as' => 'sign.in', 'uses' => 'AuthController@signIn'));
