<?php

use Illuminate\Http\Request;

class AuthController extends BaseController {

    public function __construct(\Illuminate\Http\Request $request) {
        $this->request = $request;
    }

    public function login()
    {
        return View::make('auth.login');
    }

    public function signIn()
    {
        $username = $this->request->get('username');
        $password = $this->request->get('password');

        if (Auth::attempt(array('username'=>$username, 'password'=>$password))) {
            return Redirect::to(route('tasks'));
        } else {
        }
    }



}
