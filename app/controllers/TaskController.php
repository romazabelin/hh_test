<?php

use Illuminate\Http\Request;

class TaskController extends BaseController {

    public function __construct(\Illuminate\Http\Request $request) {
        $this->request = $request;
        $this->beforeFilter('auth', array('only'=>array('index')));
    }

    public function index()
    {
        return View::make('tasks.index');
    }

    public function create()
    {}

}
